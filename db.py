import sqlite3


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class Database(object):
    """ Модель базы данных. """

    def __init__(self, db):
        self.conn = sqlite3.connect(db)
        self.conn.row_factory = dict_factory
        self.cursor = self.conn.cursor()

    def execute(self, query):
        self.cursor.execute(query)

    def execute_with_params(self, query, params):
        self.cursor.execute(query, params)

    def executemany(self, query, records):
        self.cursor.executemany(query, records)

    def commit(self):
        self.conn.commit()

    def fetchone(self):
        return self.cursor.fetchone()

    def fetchall(self):
        return self.cursor.fetchall()
