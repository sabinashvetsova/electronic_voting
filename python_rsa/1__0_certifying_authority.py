import os
import time

from helpers import get_value, write_json
from python_rsa.certifying_authority import CertifyingAuthority
from python_rsa.crypto_helpers import generate_and_save_keys

CA_DIR = get_value("CA_dir")
V_DIR = get_value("V_dir")
BD_DIR = get_value("BD_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
PRIVATE_KEYS_DIR = get_value("private_keys_dir")
PUBLIC_VOTERS_KEYS_DIR = get_value("public_voters_keys_dir")
VOTERS_CERTIFICATES_DIR = get_value("voters_certificates_dir")
CERTIFICATES_DIR = get_value("certificates_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, PUBLIC_VOTERS_KEYS_DIR), exist_ok=True
    )
    os.makedirs(os.path.join(V_DIR, PRIVATE_KEYS_DIR), exist_ok=True)
    os.makedirs(os.path.join(BD_DIR, VOTERS_CERTIFICATES_DIR), exist_ok=True)
    os.makedirs(os.path.join(V_DIR, CERTIFICATES_DIR), exist_ok=True)
    os.makedirs(CA_DIR, exist_ok=True)

    CA = CertifyingAuthority()
    CA_private_key = CA.generate_keys()

    # Считывает список избирателей
    identities = get_value("identities", "../identities.json")

    for number in range(NUMBER_OF_VOTERS):
        # Генерирует пары ключей для каждого избирателя
        _, voter_public_key = generate_and_save_keys(
            1024,
            os.path.join(
                COMMON_DIR,
                PUBLIC_KEYS_DIR,
                PUBLIC_VOTERS_KEYS_DIR,
                f"voter_{number}_public_key.pem",
            ),
            os.path.join(V_DIR, PRIVATE_KEYS_DIR, f"voter_{number}_private_key.pem"),
        )

        # Выдает сертификат для каждого зарегистрированного избирателя,
        # который содержит личность и открытый ключ избирателя.
        certificate = CA.issue_certificate(
            identities[number], voter_public_key, CA_private_key
        )
        write_json(
            certificate,
            os.path.join(
                BD_DIR, VOTERS_CERTIFICATES_DIR, f"voter_{number}_certificate.json"
            ),
        )
        write_json(
            certificate,
            os.path.join(V_DIR, CERTIFICATES_DIR, f"voter_{number}_certificate.json"),
        )

    print("It took", time.time() - start, "seconds.")
