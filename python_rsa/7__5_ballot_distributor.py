import os
import sys
import time

from helpers import get_value, write_json, read_json
from python_rsa.ballot_distributor import BallotDistributor

COMMON_DIR = get_value("common_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")


if __name__ == "__main__":
    start = time.time()

    BD = BallotDistributor()

    # 3.2.5
    # В конце периода голосования BD публикует количество выданных пустых бюллетеней и
    # серийные номера пустых бюллетеней вместе с личностью избирателей, которым они были выданы.
    voters = BD.get_voters()
    write_json(voters, os.path.join(COMMON_DIR, "BD_ballots.json"))

    # Число пустых бюллетеней должно быть больше или равно количеству полученных голосов,
    # но меньше или равно количеству зарегистрированных избирателей.
    votes = read_json(os.path.join(COMMON_DIR, "VC_votes.json"))

    ballots_count = len(voters)
    votes_count = len(votes)

    if ballots_count < votes_count or ballots_count > NUMBER_OF_VOTERS:
        print("Ошибка: число пустых бюллетеней вышло за рамки.")
        sys.exit()

    print("It took", time.time() - start, "seconds.")
