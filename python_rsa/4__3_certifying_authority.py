import json
import os
import sys
import time

import rsa

from helpers import get_value, read_bytes, write_json, read_json
from python_rsa.certifying_authority import CertifyingAuthority
from python_rsa.crypto_helpers import encrypt, decrypt, verify_digest_signature

CA_DIR = get_value("CA_dir")
V_DIR = get_value("V_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")
VOTER_MARKS_AND_CERTS_DIR = get_value("voter_marks_and_certs_dir")
PUBLIC_VOTERS_KEYS_DIR = get_value("public_voters_keys_dir")
SIGNED_VOTER_MARKS_DIR = get_value("signed_voter_marks_dir")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(os.path.join(V_DIR, SIGNED_VOTER_MARKS_DIR), exist_ok=True)

    # Считывает открытый ключ BD и закрытый ключ CA
    pem_BD_public_key = read_bytes(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "BD_public_key.pem")
    )
    BD_public_key = rsa.PublicKey.load_pkcs1(pem_BD_public_key)

    pem_CA_private_key = read_bytes(os.path.join(CA_DIR, "CA_private_key.pem"))
    CA_private_key = rsa.PrivateKey.load_pkcs1(pem_CA_private_key)

    # Считывает список избирателей
    identities = get_value("identities", "../identities.json")

    CA = CertifyingAuthority()

    # Создает таблицу избирателей
    CA.create_voters_table()

    for number in range(NUMBER_OF_VOTERS):
        # Считывает открытый ключ избирателя
        pem_voter_public_key = read_bytes(
            os.path.join(
                COMMON_DIR,
                PUBLIC_KEYS_DIR,
                PUBLIC_VOTERS_KEYS_DIR,
                f"voter_{number}_public_key.pem",
            )
        )
        voter_public_key = rsa.PublicKey.load_pkcs1(pem_voter_public_key)

        # Считывает сертификат и метку избирателя с их дайджестами
        message_to_CA = read_json(
            os.path.join(
                CA_DIR, VOTER_MARKS_AND_CERTS_DIR, f"voter_{number}_mark_and_cert.json"
            )
        )

        # 3.2.3
        # Сначала проверяет, является ли избиратель зарегистрированным, проверяя подтвержденный сертификат BD
        # для избирателя, сопровождающий голос.
        encrypted_certificate_digest = message_to_CA.get("encrypted_certificate_digest")
        certificate_digest = decrypt(encrypted_certificate_digest, CA_private_key)

        voter_certificate = message_to_CA.get("encrypted_voter_certificate")
        decrypted_voter_certificate = json.loads(
            decrypt(voter_certificate, CA_private_key).decode("utf-8")
        )

        is_certificate_valid = verify_digest_signature(
            json.dumps(decrypted_voter_certificate).encode("utf-8"),
            certificate_digest,
            BD_public_key,
        )
        if not is_certificate_valid:
            print("Сертификат не валиден.")
            sys.exit()

        # CA также следит за тем, чтобы избиратель не подал ранее, еще одну соленую метку избирателя для подписи.
        voter_mark = message_to_CA.get("encrypted_voter_mark")
        salted_voter_mark = decrypt(voter_mark, CA_private_key)
        duplicates = CA.check_voter_mark_duplicates(salted_voter_mark)
        if duplicates:
            print("Такая метка избирателя уже была подана.")
            sys.exit()

        # Проверяя подпись избирателя на дайджесте соленой марки избирателя, CA также удостоверяется,
        # что метка избирателя не была подделана при транспортировке.
        encrypted_mark_digest = message_to_CA.get("encrypted_mark_digest")
        mark_digest = decrypt(encrypted_mark_digest, CA_private_key)
        is_voter_mark_valid = verify_digest_signature(
            salted_voter_mark, mark_digest, voter_public_key
        )
        if not is_voter_mark_valid:
            print("Метка избирателя не валидна.")
            sys.exit()

        # Затем CA подписывает соленую метку избирателя и возвращает ее избирателю,
        # зашифрованную открытым ключом избирателя.
        salted_voter_mark = salted_voter_mark.decode("utf-8")
        signed_voter_mark = CA.sign_voter_mark(int(salted_voter_mark), CA_private_key)
        encrypted_voter_mark = encrypt(
            signed_voter_mark.encode("utf-8"), voter_public_key
        )
        write_json(
            encrypted_voter_mark,
            os.path.join(
                V_DIR, SIGNED_VOTER_MARKS_DIR, f"voter_{number}_voter_mark.json"
            ),
        )

        # Удостоверяющий орган хранит копию соленой метки избирателя, подписанной избирателем,
        # вместе с личностью избирателя, которая должна быть опубликована в конце голосования.
        CA.add_new_voter(
            identities[number], salted_voter_mark, mark_digest, signed_voter_mark
        )

    print("It took", time.time() - start, "seconds.")
