import base64
import hashlib

import rsa
from Crypto.Cipher import AES

from helpers import write_bytes


def generate_and_save_keys(keys_length, public_key_path, private_key_path):
    """
    Генерирует открытый и закрытый ключи и сохраняет их в файлах в PEM-формате.
    :param keys_length: длина ключей
    :param public_key_path: путь до файла с открытым ключом
    :param private_key_path: путь до файла с закрытым ключом
    :return: объект с закрытым ключом и открытый ключ в PEM-формате
    """

    (public_key, private_key) = rsa.newkeys(keys_length, poolsize=8)

    pem_private_key = private_key.save_pkcs1()
    pem_public_key = public_key.save_pkcs1()

    write_bytes(pem_public_key, public_key_path)
    write_bytes(pem_private_key, private_key_path)

    return private_key, pem_public_key


def encrypt(bytes_data, public_key):
    """
    Зашифровывает сообщение открытым ключом получателя.
    :param bytes_data: сообщение
    :param public_key: открытый ключ получателя
    :return:
    """
    # Генерирует случайный ключ для алгоритма AES
    aes_key = rsa.randnum.read_random_bits(128)

    # Использует ключ для шифрования данных с помощью алгоритма AES
    cipher = AES.new(aes_key, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, tag = cipher.encrypt_and_digest(bytes_data)

    # Зашифровывает ключ для AES по алгоритму RSA
    encrypted_aes_key = rsa.encrypt(aes_key, public_key)

    # Возвращает зашифрованные данные и зашифрованный ключ
    return {
        "encrypted_aes_key": base64.b64encode(encrypted_aes_key).decode("utf-8"),
        "nonce": base64.b64encode(nonce).decode("utf-8"),
        "ciphertext": base64.b64encode(ciphertext).decode("utf-8"),
        "tag": base64.b64encode(tag).decode("utf-8"),
    }


def decrypt(data, private_key):
    """
    Расшифровывает сообщение с помощью закрытого ключа.
    :param data: сообщение
    :param private_key: закрытый ключ
    :return:
    """
    encrypted_aes_key = base64.b64decode(data.get("encrypted_aes_key").encode("utf-8"))
    aes_key = rsa.decrypt(encrypted_aes_key, private_key)

    nonce = base64.b64decode(data.get("nonce").encode("utf-8"))
    cipher = AES.new(aes_key, AES.MODE_EAX, nonce=nonce)

    ciphertext = base64.b64decode(data.get("ciphertext").encode("utf-8"))
    plaintext = cipher.decrypt(ciphertext)

    try:
        tag = base64.b64decode(data.get("tag").encode("utf-8"))
        cipher.verify(tag)
        return plaintext

    except ValueError as e:
        print(e)
        return None


def sign_message(message, key):
    """
    Подписывает сообщение.
    :param message: сообщение
    :param key: закрытый ключ
    :return:
    """
    return rsa.sign(message, key, "MD5")


def sign_message_digest(message, key):
    """
    Подписывает дайджест сообщения.
    :param message: сообщение
    :param key: закрытый ключ
    :return:
    """
    message_digest = hashlib.md5(message).hexdigest().encode("utf-8")
    signature = rsa.sign(message_digest, key, "MD5")
    return signature


def verify_message_signature(message, signature, key):
    """
    Проверяет подпись на сообщении.
    :param message: сообщение
    :param signature: подпись
    :param key: открытый ключ
    :return:
    """
    try:
        rsa.verify(message, signature, key)
    except rsa.pkcs1.VerificationError:
        return False
    return True


def verify_digest_signature(message, signature, key):
    """
    Проверяет подпись дайджеста сообщения.
    :param message: сообщение
    :param signature: подпись
    :param key: открытый ключ
    :return:
    """
    message = hashlib.md5(message).hexdigest().encode("utf-8")
    try:
        rsa.verify(message, signature, key)
    except rsa.pkcs1.VerificationError:
        return False
    return True
