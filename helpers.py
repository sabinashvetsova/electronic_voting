import json
import sys

CONFIG_FILE_PATH = "../config.json"


def read_json(path):
    try:
        with open(path, encoding="utf-8") as f:
            data = json.load(f)
        return data
    except ValueError:
        print("В файле содержатся данные в неправильном формате.")
        sys.exit()


def write_json(dict, path):
    with open(path, "w", encoding="utf-8") as f:
        json.dump(dict, f, indent=4, ensure_ascii=False)


def get_value(key, file=CONFIG_FILE_PATH):
    data = read_json(file)
    return data[key]


def read_bytes(path):
    with open(path, "rb") as f:
        return f.read()


def write_bytes(bytes, path):
    with open(path, "wb") as f:
        f.write(bytes)


def gcd(p: int, q: int) -> int:
    while q != 0:
        (p, q) = (q, p % q)
    return p


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception("modular inverse does not exist")
    else:
        return x % m
