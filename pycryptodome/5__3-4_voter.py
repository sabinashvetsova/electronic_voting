import json
import os
import random
import sys
import time

from Crypto.PublicKey import RSA

from helpers import get_value, read_json, read_bytes, write_json
from pycryptodome.crypto_helpers import generate_and_save_keys, decrypt, encrypt
from pycryptodome.voter import Voter

V_DIR = get_value("V_dir")
VC_DIR = get_value("VC_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")
SIGNED_VOTER_MARKS_DIR = get_value("signed_voter_marks_dir")
NUMBER_OF_OPTIONS = get_value("number_of_options")
ENCRYPTED_VOTES_DIR = get_value("encrypted_votes_dir")
PRIVATE_KEYS_DIR = get_value("private_keys_dir")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(os.path.join(VC_DIR, ENCRYPTED_VOTES_DIR), exist_ok=True)

    # Генерирует пары ключей для VC
    _, pem_public_key = generate_and_save_keys(
        1024,
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, f"VC_public_key.pem"),
        os.path.join(VC_DIR, f"VC_private_key.pem"),
    )
    VC_public_key = RSA.import_key(pem_public_key)

    # Считывает публичный ключ CA
    pem_CA_public_key = read_bytes(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "CA_public_key.pem")
    )
    CA_public_key = RSA.import_key(pem_CA_public_key)

    V = Voter()

    for number in range(NUMBER_OF_VOTERS):
        # Считывает закрытый ключ избирателя
        pem_voter_private_key = read_bytes(
            os.path.join(V_DIR, PRIVATE_KEYS_DIR, f"voter_{number}_private_key.pem")
        )
        voter_private_key = RSA.import_key(pem_voter_private_key)

        # 3.2.3
        # Избиратель «снимает соль» с подписанной соленой метки и получает подпись СА на метке избирателя.
        encrypted_voter_mark = read_json(
            os.path.join(
                V_DIR, SIGNED_VOTER_MARKS_DIR, f"voter_{number}_voter_mark.json"
            )
        )
        salt = V.get_voter(number + 1)["salt"]

        signed_salted_voter_mark = decrypt(encrypted_voter_mark, voter_private_key)
        signed_voter_mark = V.unsalt_voter_mark(
            int(signed_salted_voter_mark.decode("utf-8")), salt, CA_public_key
        )

        # Избиратель удостоверяется, что удостоверяющий орган не поставил какой-либо опознавательный знак
        # на метке избирателя.
        voter_mark = V.get_voter(number + 1)["voter_mark"]

        is_signed_voter_mark_valid = V.check_signed_voter_mark(
            signed_voter_mark, CA_public_key, voter_mark
        )
        if not is_signed_voter_mark_valid:
            print("Подписанная метка избирателя не валидна.")
            sys.exit()

        # 3.2.4
        # Теперь избиратель готовит сообщение фиксированной длины заранее определенного формата (формат объявляется
        # всем избирателям до начала голосования) и указывает свой голос в качестве содержания сообщения.
        vote = str(random.randint(1, NUMBER_OF_OPTIONS))

        # Избиратель добавляет к этому сообщению свою подписанную метку избирателя (подписанную CA)
        # и создает дайджест сообщения о голосе и подписанной метке избирателя.
        vote_message = V.get_vote_message(vote, signed_voter_mark)

        # Затем избиратель зашифровывает их с помощью открытого ключа VC
        encrypted_vote_message = encrypt(
            json.dumps(vote_message).encode("utf-8"), VC_public_key
        )

        # и отправляет их VC.
        write_json(
            encrypted_vote_message,
            os.path.join(VC_DIR, ENCRYPTED_VOTES_DIR, f"voter_{number}_vote.json"),
        )

        V.update_signed_voter_mark(signed_voter_mark, number + 1)

    print("It took", time.time() - start, "seconds.")
