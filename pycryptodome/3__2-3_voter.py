import json
import os
import sys
import time

from Crypto.PublicKey import RSA

from helpers import get_value, write_json, read_bytes, read_json
from pycryptodome.crypto_helpers import (
    decrypt,
    sign_message_digest,
    encrypt,
)
from pycryptodome.voter import Voter

CA_DIR = get_value("CA_dir")
V_DIR = get_value("V_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
PRIVATE_KEYS_DIR = get_value("private_keys_dir")
CERTIFICATES_DIR = get_value("certificates_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")
ENCRYPTED_BALLOTS_DIR = get_value("encrypted_ballots_dir")
SIGNED_CERTS_DIR = get_value("signed_certs_dir")
VOTER_MARKS_AND_CERTS_DIR = get_value("voter_marks_and_certs_dir")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(os.path.join(CA_DIR, VOTER_MARKS_AND_CERTS_DIR), exist_ok=True)

    pem_BD_public_key = read_bytes(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "BD_public_key.pem")
    )
    BD_public_key = RSA.import_key(pem_BD_public_key)

    pem_CA_public_key = read_bytes(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "CA_public_key.pem")
    )
    CA_public_key = RSA.import_key(pem_CA_public_key)

    V = Voter()

    # Создает таблицу избирателей
    V.create_voters_table()

    for number in range(NUMBER_OF_VOTERS):
        # Считывает закрытый ключ избирателя
        pem_voter_private_key = read_bytes(
            os.path.join(V_DIR, PRIVATE_KEYS_DIR, f"voter_{number}_private_key.pem")
        )
        voter_private_key = RSA.import_key(pem_voter_private_key)

        # Считывает зашифрованный бюллетень
        encrypted_ballot = read_json(
            os.path.join(V_DIR, ENCRYPTED_BALLOTS_DIR, f"voter_{number}_ballot.json")
        )

        # Расшифровывает бюллетень
        decrypted_ballot = json.loads(
            decrypt(encrypted_ballot, voter_private_key).decode("utf-8")
        )

        # 3.2.2
        # Когда избиратель получает сообщение, он следит за тем, чтобы во время транзита бюллетень не был подделан,
        # включая то, что никто не поставил в нем опознавательный знак
        clear_ballot = V.check_ballot(decrypted_ballot, BD_public_key)
        if not clear_ballot:
            print("Бюллетень испорчен.")
            sys.exit()

        # 3.2.3
        # Используя серийный номер y, избиратель создает метку избирателя m
        m = V.generate_voter_mark(clear_ballot.get("serial_number"))

        # Затем избиратель солит метку избирателя случайным числом r, чтобы получить соленую метку избирателя
        salt, salted_voter_mark = V.salt_voter_mark(int(m), CA_public_key)

        # Избиратель также вычисляет дайджест соленой метки избирателя и подписывает дайджест
        signed_digest = sign_message_digest(
            salted_voter_mark.encode("utf-8"), voter_private_key
        )

        # Избиратель зашифровывает соленую метку избирателя и подписанный дайджест соленой отметки избирателя
        # открытым ключом СА
        encrypted_voter_mark = encrypt(salted_voter_mark.encode("utf-8"), CA_public_key)
        encrypted_mark_digest = encrypt(signed_digest, CA_public_key)

        # Считывает и зашифровывает открытым ключом CA сертификат избирателя
        certificate = read_json(
            os.path.join(V_DIR, CERTIFICATES_DIR, f"voter_{number}_certificate.json")
        )

        encrypted_voter_certificate = encrypt(
            json.dumps(certificate).encode("utf-8"), CA_public_key
        )

        # Считывает и зашифровывает подписанный дайджест сертификата избирателя, который он получил от BD
        signed_certificate_digest = read_bytes(
            os.path.join(V_DIR, SIGNED_CERTS_DIR, f"voter_{number}_signed_cert.txt")
        )
        encrypted_certificate_digest = encrypt(signed_certificate_digest, CA_public_key)

        # Собирает сообщение и отправляет CA
        message_to_CA = {
            "encrypted_voter_mark": encrypted_voter_mark,
            "encrypted_mark_digest": encrypted_mark_digest,
            "encrypted_voter_certificate": encrypted_voter_certificate,
            "encrypted_certificate_digest": encrypted_certificate_digest,
        }
        write_json(
            message_to_CA,
            os.path.join(
                CA_DIR, VOTER_MARKS_AND_CERTS_DIR, f"voter_{number}_mark_and_cert.json"
            ),
        )

        V.add_new_voter(salt=salt, voter_mark=m)

    print("It took", time.time() - start, "seconds.")
