import base64
import json
import os
import sys
import time

from Crypto.PublicKey import RSA

from helpers import get_value, read_json, read_bytes, write_bytes, write_json
from pycryptodome.crypto_helpers import decrypt, sign_message
from pycryptodome.vote_compiler import VoteCompiler

VC_DIR = get_value("VC_dir")
COMMON_DIR = get_value("common_dir")
SIGNED_VOTES_DIGESTS_DIR = get_value("signed_votes_digests_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")
NUMBER_OF_OPTIONS = get_value("number_of_options")
ENCRYPTED_VOTES_DIR = get_value("encrypted_votes_dir")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(os.path.join(COMMON_DIR, SIGNED_VOTES_DIGESTS_DIR), exist_ok=True)

    # Считывает закрытый ключ VC
    pem_VC_private_key = read_bytes(os.path.join(VC_DIR, "VC_private_key.pem"))
    VC_private_key = RSA.import_key(pem_VC_private_key)

    # Создает таблицу избирателей
    VC = VoteCompiler()
    VC.create_voters_table()

    votes = dict.fromkeys([str(x) for x in range(1, NUMBER_OF_OPTIONS + 1)], 0)

    for number in range(NUMBER_OF_VOTERS):
        # 3.2.4
        # VC считывает отданный голос
        encrypted_vote = read_json(
            os.path.join(VC_DIR, ENCRYPTED_VOTES_DIR, f"voter_{number}_vote.json")
        )

        # Расшифровывает голос
        decrypted_vote = json.loads(
            decrypt(encrypted_vote, VC_private_key).decode("utf-8")
        )
        vote = decrypted_vote.get("vote")
        signed_voter_mark = base64.b64decode(
            decrypted_vote.get("signed_voter_mark").encode("utf-8")
        ).decode("utf-8")
        digest = base64.b64decode(decrypted_vote.get("digest").encode("utf-8"))

        # VC следит за тем, чтобы голос не был подделан во время загрузки избирателем или скачивания.
        is_vote_valid = VC.check_vote(vote, signed_voter_mark, digest)
        if not is_vote_valid:
            print("Голос подделан.")
            sys.exit()

        # Затем VC проверяет, что он не получил эту метку избирателя раньше.
        duplicates = VC.check_voter_mark_duplicates(signed_voter_mark)
        if duplicates:
            print("Такая метка избирателя уже была подана.")
            sys.exit()

        # Затем VC подписывает дайджест голоса и метки избирателя и загружает их в общественное место.
        digest_signature = sign_message(digest, VC_private_key)
        write_bytes(
            digest_signature,
            os.path.join(
                COMMON_DIR,
                SIGNED_VOTES_DIGESTS_DIR,
                f"voter_{number}_signed_vote_digest.txt",
            ),
        )

        VC.add_new_voter(signed_voter_mark, vote, digest)

        votes[vote] += 1

    # 3.2.5
    # В конце периода голосования VC публикует все голоса вместе с соответствующими метками избирателей,
    # проиндексированными дайджестами голосов и меток избирателей.
    voters = VC.get_voters()
    write_json(voters, os.path.join(COMMON_DIR, "VC_votes.json"))

    # VC объявляет соответствующую статистику голосования, которую он рассчитал.
    write_json(votes, os.path.join(COMMON_DIR, "VC_voting_results.json"))

    print("It took", time.time() - start, "seconds.")
