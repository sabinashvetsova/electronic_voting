import os
import sys
import time

from helpers import get_value, write_json, read_json
from pycryptodome.certifying_authority import CertifyingAuthority

COMMON_DIR = get_value("common_dir")

if __name__ == "__main__":
    start = time.time()

    CA = CertifyingAuthority()

    # 3.2.5
    # В конце периода голосования СА публикует набор соленых меток избирателей, полученных от избирателя,
    # их дайджесты, подписанные избирателями, и соответствующие соленые метки избирателей,
    # подписанные СА, индексируемые личностью избирателей.
    voters = CA.get_voters()
    write_json(voters, os.path.join(COMMON_DIR, "CA_salted_voter_marks.json"))

    # Количество таких наборов должно быть больше или равно количеству поданных голосов,
    # но должно быть меньше или равно количеству пустых бюллетеней, выданных BD.
    votes = read_json(os.path.join(COMMON_DIR, "VC_votes.json"))
    ballots = read_json(os.path.join(COMMON_DIR, "BD_ballots.json"))

    salted_voter_marks_count = len(voters)
    votes_count = len(votes)
    ballots_count = len(ballots)

    if (
        salted_voter_marks_count < votes_count
        or salted_voter_marks_count > ballots_count
    ):
        print("Ошибка: количество соленых меток избирателей вышло за рамки.")
        sys.exit()

    print("It took", time.time() - start, "seconds.")
