import base64
import hashlib

from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.Hash import MD5
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Signature import pkcs1_15

from helpers import write_bytes


def generate_and_save_keys(keys_length, public_key_path, private_key_path):
    """
    Генериует открытый и закрытый ключи и сохраняет их в файлах в PEM-формате.
    :param keys_length: длина ключей
    :param public_key_path: путь до файла с открытым ключом
    :param private_key_path: путь до файла с закрытым ключом
    :return: закрытый и открытый ключ в PEM-формате
    """
    private_key = RSA.generate(keys_length)

    pem_private_key = private_key.export_key()
    pem_public_key = private_key.publickey().export_key()

    write_bytes(pem_private_key, private_key_path)
    write_bytes(pem_public_key, public_key_path)

    return private_key, pem_public_key


def encrypt(bytes_data, public_key):
    """
    Зашифровывает сообщение открытым ключом получателя.
    :param bytes_data: сообщение
    :param public_key: открытый ключ получателя
    :return:
    """
    # Генерирует случайный ключ для алгоритма AES
    aes_key = get_random_bytes(16)

    # Использует ключ для шифрования данных с помощью алгоритма AES
    cipher = AES.new(aes_key, AES.MODE_EAX)
    nonce = cipher.nonce
    ciphertext, tag = cipher.encrypt_and_digest(bytes_data)

    # Зашифровывает ключ для AES по алгоритму RSA
    cipher_rsa = PKCS1_OAEP.new(public_key)
    encrypted_aes_key = cipher_rsa.encrypt(aes_key)

    # Возвращает зашифрованные данные и зашифрованный ключ
    return {
        "encrypted_aes_key": base64.b64encode(encrypted_aes_key).decode("utf-8"),
        "nonce": base64.b64encode(nonce).decode("utf-8"),
        "ciphertext": base64.b64encode(ciphertext).decode("utf-8"),
        "tag": base64.b64encode(tag).decode("utf-8"),
    }


def decrypt(data, private_key):
    """
    Расшифровывает сообщение с помощью закрытого ключа.
    :param data: сообщение
    :param private_key: закрытый ключ
    :return:
    """
    encrypted_aes_key = base64.b64decode(data.get("encrypted_aes_key").encode("utf-8"))
    cipher_rsa = PKCS1_OAEP.new(private_key)
    aes_key = cipher_rsa.decrypt(encrypted_aes_key)

    nonce = base64.b64decode(data.get("nonce").encode("utf-8"))
    cipher = AES.new(aes_key, AES.MODE_EAX, nonce=nonce)

    ciphertext = base64.b64decode(data.get("ciphertext").encode("utf-8"))
    plaintext = cipher.decrypt(ciphertext)

    try:
        tag = base64.b64decode(data.get("tag").encode("utf-8"))
        cipher.verify(tag)
        return plaintext

    except ValueError as e:
        print(e)
        return None


def sign_message(message, key):
    """
    Подписывает сообщение.
    :param message: сообщение
    :param key: закрытый ключ
    :return:
    """
    signer = pkcs1_15.new(key)
    hash = MD5.new(data=message)
    signature = signer.sign(hash)
    return signature


def sign_message_digest(message, key):
    """
    Подписывает дайджест сообщения.
    :param message: сообщение
    :param key: закрытый ключ
    :return:
    """
    message_digest = hashlib.md5(message).hexdigest().encode("utf-8")
    signer = pkcs1_15.new(key)
    hash = MD5.new(data=message_digest)
    signature = signer.sign(hash)
    return signature


def verify_message_signature(message, signature, key):
    """
    Проверяет подпись на сообщении.
    :param message: сообщение
    :param signature: подпись
    :param key: открытый ключ
    :return:
    """
    try:
        verifier = pkcs1_15.new(key)
        hash = MD5.new(data=message)
        verifier.verify(hash, signature)
    except ValueError:
        return False
    return True


def verify_digest_signature(message, signature, key):
    """
    Проверяет подпись дайджеста сообщения.
    :param message: сообщение
    :param signature: подпись
    :param key: открытый ключ
    :return:
    """
    message = hashlib.md5(message).hexdigest().encode("utf-8")
    try:
        verifier = pkcs1_15.new(key)
        hash = MD5.new(data=message)
        verifier.verify(hash, signature)
    except ValueError:
        return False
    return True
