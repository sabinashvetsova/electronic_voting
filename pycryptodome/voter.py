import base64
import hashlib
import os
import random

import numpy as np

from db import Database
from helpers import gcd, modinv, get_value
from pycryptodome.crypto_helpers import verify_message_signature

V_DIR = get_value("V_dir")
NUMBER_OF_OPTIONS = get_value("number_of_options")
DB_NAME = get_value("db_name")
DB = os.path.join(V_DIR, DB_NAME)


class Voter:
    @staticmethod
    def _check_ballot_has_certain_fields(ballot):
        """
        Проверяет, что бюллетень строго состоит из 3 полей: y, хеш и подпись.
        Если есть другие поля, их убирает.
        :param ballot: бюллетень
        :return: очищенный бюллетень, если его можно восстановить. False - в обратном случае.
        """

        keys = ballot.keys()

        if len(keys) < 3:
            return False

        correct_keys = ["serial_number", "digest", "signature"]
        clear_ballot = {}

        for key in correct_keys:
            if key not in ballot:
                return False
            clear_ballot[key] = ballot[key]

        return clear_ballot

    @staticmethod
    def _verify_ballot_signature(ballot, bd_public_key):
        """
        Проверяет цифровую подпись BD: вычисляет хеш и проверяет подпись.
        :param ballot: бюллетень
        :param bd_public_key: открытый ключ BD
        :return: True, если подпись корректная. False - в обратном случае.
        """

        serial_number = ballot.get("serial_number")

        # берёт хеш от серийного номера
        bytes_serial_number = str(serial_number).encode()
        digest = hashlib.md5(bytes_serial_number).hexdigest()

        if ballot.get("digest") != digest:
            return False

        signature = base64.b64decode(str.encode(ballot.get("signature")))

        return verify_message_signature(digest.encode(), signature, bd_public_key)

    def check_ballot(self, ballot, bd_public_key):
        """
        Проверяет, что бюллетень не подделали.
        :param ballot: бюллетень
        :param bd_public_key: открытый ключ BD
        :return: Очищенный бюллетень, если он не подделан. False - в обратном случае.
        """

        clear_ballot = self._check_ballot_has_certain_fields(ballot)

        if clear_ballot and self._verify_ballot_signature(clear_ballot, bd_public_key):
            return clear_ballot

        return False

    @staticmethod
    def generate_voter_mark(serial_number):
        """
        Создает метку избирателя.
        :param serial_number: серийный номер
        :return: метка избирателя
        """
        random_length = 5

        # Избиратель дополняет y случайным числом фиксированной длины, чтобы получить число x.
        rand_number = "".join(
            [str(random.randint(0, 10)) for _ in range(random_length)]
        )
        x = list(str(serial_number) + rand_number)

        # Затем избиратель вычисляет труднообращаемую перестановку m x.
        return "".join(list(np.random.permutation(x)))

    @staticmethod
    def salt_voter_mark(voter_mark, public_key):
        """
        Получает соленую метку избирателя.
        :param voter_mark: метка избирателя
        :param public_key: открытый ключ подписывающего лица
        :return: соленая метка
        """
        flag = False
        r = 0
        while not flag:
            r = random.randint(1, 100)
            if gcd(r, public_key.n) == 1:
                flag = True
        x = r ** public_key.e * voter_mark % public_key.n
        return r, str(x)

    @staticmethod
    def unsalt_voter_mark(signed_salted_voter_mark, salt, public_key):
        """
        Снимает соль с соленой метки избирателя.
        :param signed_salted_voter_mark: подписанная соленая метка избирателя
        :param salt: соль
        :param public_key: открытый ключ подписывающего лица
        :return: соленая метка
        """
        inv_r = modinv(salt, public_key.n)
        return str(inv_r * signed_salted_voter_mark % public_key.n)

    @staticmethod
    def check_signed_voter_mark(signed_voter_mark, ca_public_key, voter_mark):
        """
        Проверяет подпись CA на метке избирателя.
        :param signed_voter_mark: подписанная метка избирателя
        :param ca_public_key: открытый ключ CA
        :param voter_mark: метка избирателя
        :return: True, если валидна. False - в обратном случае.
        """

        return voter_mark == pow(
            int(signed_voter_mark), ca_public_key.e, ca_public_key.n
        )

    @staticmethod
    def get_vote_message(vote, signed_voter_mark):
        """
        Получает сообщение, состоящее из голоса, подписанной метки избирателя и их дайджеста.
        :param vote: голос
        :param signed_voter_mark: подписанная метка избирателя
        :return: сообщение
        """

        digest = hashlib.md5((vote + signed_voter_mark).encode("utf-8")).hexdigest()

        return {
            "vote": vote,
            "signed_voter_mark": base64.b64encode(
                signed_voter_mark.encode("utf-8")
            ).decode("utf-8"),
            "digest": base64.b64encode(digest.encode("utf-8")).decode("utf-8"),
        }

    @staticmethod
    def get_signed_voter_mark(voters, number):
        """
        Получает подписанную метку избирателя
        :param voters: все избиратели
        :param number: номер избирателя
        """

        for voter in voters:
            if voter["id"] == number + 1:
                return voter["signed_voter_mark"]
        return None

    @staticmethod
    def check_vote_publication(votes, voter_signed_voter_mark):
        """
        Проверяет засчитан ли голос в итоговом подсчете
        :param votes: все голоса
        :param voter_signed_voter_mark: подписанная метка избирателя
        :return: True, если засчитан. False - в обратном случае.
        """

        for vote in votes:
            if vote["signed_voter_mark"] == voter_signed_voter_mark:
                return True
        return False

    @staticmethod
    def check_voting_statistics(votes, voting_results):
        """
        Проверяет правильно, ли подсчитаны голоса
        :param votes: все голоса
        :param voting_results: результаты голосования
        :return: True, если правильно. False - в обратном случае.
        """

        calculated_statistic = dict.fromkeys(
            [str(x) for x in range(1, NUMBER_OF_OPTIONS + 1)], 0
        )

        for vote in votes:
            calculated_statistic[vote["vote"]] += 1

        return calculated_statistic == voting_results

    @staticmethod
    def create_voters_table():
        """
        Создаёт таблицу с избирателями, если её не существует.
        :return:
        """

        db = Database(DB)
        db.execute("DROP TABLE IF EXISTS V_voters;")
        db.execute(
            """CREATE TABLE V_voters (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            salt INTEGER NOT NULL,
            voter_mark INTEGER NOT NULL,
            signed_voter_mark TEXT);"""
        )

    @staticmethod
    def add_new_voter(salt, voter_mark):
        """
        Сохраняет в БД соль и метку избирателя.
        :param salt: соль
        :param voter_mark: метка избирателя
        :return:
        """

        insert_query = "INSERT INTO V_voters (salt, voter_mark) VALUES (?, ?);"

        db = Database(DB)
        db.execute_with_params(insert_query, (salt, voter_mark))
        db.commit()

    @staticmethod
    def get_voter(id):
        """
        Извлекает из таблицы определенного избирателя.
        :param id: id избирателя
        :return:
        """

        select_query = (
            "SELECT id, salt, voter_mark, signed_voter_mark FROM V_voters WHERE id=?;"
        )

        db = Database(DB)
        db.execute_with_params(select_query, (id,))
        return db.fetchone()

    @staticmethod
    def update_signed_voter_mark(signed_voter_mark, id):
        """
        Сохраняет в БД подписанную метку избирателя.
        :param signed_voter_mark: подписанная метка избирателя
        :param id: id избирателя
        :return:
        """

        insert_query = "UPDATE V_voters SET signed_voter_mark=? WHERE id=?;"

        db = Database(DB)
        db.execute_with_params(insert_query, (signed_voter_mark, id))
        db.commit()
