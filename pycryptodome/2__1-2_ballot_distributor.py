import base64
import json
import os
import sys
import time

from Crypto.PublicKey import RSA

from helpers import get_value, read_bytes, write_bytes, write_json, read_json
from pycryptodome.ballot_distributor import BallotDistributor
from pycryptodome.crypto_helpers import (
    sign_message_digest,
    encrypt,
)

V_DIR = get_value("V_dir")
BD_DIR = get_value("BD_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
VOTERS_CERTIFICATES_DIR = get_value("voters_certificates_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")
ENCRYPTED_BALLOTS_DIR = get_value("encrypted_ballots_dir")
SIGNED_CERTS_DIR = get_value("signed_certs_dir")


if __name__ == "__main__":
    start = time.time()

    # Создает необходимые папки
    os.makedirs(os.path.join(V_DIR, ENCRYPTED_BALLOTS_DIR), exist_ok=True)
    os.makedirs(os.path.join(V_DIR, SIGNED_CERTS_DIR), exist_ok=True)

    BD = BallotDistributor()
    BD_private_key = BD.generate_keys()

    # Считывает публичный ключ CA
    pem_CA_public_key = read_bytes(
        os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "CA_public_key.pem")
    )
    CA_public_key = RSA.import_key(pem_CA_public_key)

    # Создает таблицу избирателей
    BD.create_voters_table()

    # Считывает список избирателей
    identities = get_value("identities", "../identities.json")

    # Генерирует уникальный серийный номер y для каждого избирателя
    serial_numbers = BD.generate_serial_numbers(NUMBER_OF_VOTERS)

    # и создает список серийных номеров избирательных бюллетеней и идентификаторов избирателей
    BD.add_new_voters(identities, serial_numbers)

    for number in range(NUMBER_OF_VOTERS):
        # 3.2.1
        # Считывает сертификат, выданный регистрирующим органом
        certificate = read_json(
            os.path.join(
                BD_DIR, VOTERS_CERTIFICATES_DIR, f"voter_{number}_certificate.json"
            )
        )

        # Проверяет сертификат избирателя
        is_certificate_valid = BD.check_certificate(certificate, CA_public_key)
        if not is_certificate_valid:
            print("Сертификат не валиден.")
            sys.exit()

        # Подписывает дайджест сертификата избирателя и отправляет его избирателю
        signed_certificate_digest = sign_message_digest(
            json.dumps(certificate).encode("utf-8"), BD_private_key
        )
        write_bytes(
            signed_certificate_digest,
            os.path.join(V_DIR, SIGNED_CERTS_DIR, f"voter_{number}_signed_cert.txt"),
        )

        # 3.2.2
        # Создает бюллетень и отправляет его в зашифрованном виде с помощью открытого ключа избирателя
        ballot = BD.get_ballot(serial_numbers[number], BD_private_key)

        pem_public_key = base64.b64decode(certificate.get("public_key").encode("utf-8"))
        voter_public_key = RSA.import_key(pem_public_key)

        encrypted_ballot = encrypt(json.dumps(ballot).encode("utf-8"), voter_public_key)
        write_json(
            encrypted_ballot,
            os.path.join(V_DIR, ENCRYPTED_BALLOTS_DIR, f"voter_{number}_ballot.json"),
        )

    print("It took", time.time() - start, "seconds.")
