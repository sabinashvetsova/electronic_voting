import os
import sys
import time

from helpers import get_value, read_json
from pycryptodome.voter import Voter

COMMON_DIR = get_value("common_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")


if __name__ == "__main__":
    start = time.time()

    V = Voter()

    votes = read_json(os.path.join(COMMON_DIR, "VC_votes.json"))
    voting_results = read_json(os.path.join(COMMON_DIR, "VC_voting_results.json"))

    for number in range(NUMBER_OF_VOTERS):
        # 3.2.5
        # Избиратель, желающий получить гарантию того, что его или ее голос засчитан в итоговом подсчете,
        # следит за тем, чтобы его или ее голос присутствовал в списке опубликованных голосов.

        signed_voter_mark = V.get_voter(number + 1)["signed_voter_mark"]

        is_vote_published = V.check_vote_publication(votes, signed_voter_mark)
        if not is_vote_published:
            print("Голос не засчитан.")
            sys.exit()

        is_voting_statistics_correct = V.check_voting_statistics(votes, voting_results)
        if not is_voting_statistics_correct:
            print("Статистика голосования некорректная.")
            sys.exit()

    print("It took", time.time() - start, "seconds.")
