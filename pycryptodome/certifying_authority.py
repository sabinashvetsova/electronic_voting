import base64
import os

from db import Database
from helpers import get_value
from pycryptodome.crypto_helpers import generate_and_save_keys, sign_message

CA_DIR = get_value("CA_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
DB_NAME = get_value("db_name")
DB = os.path.join(CA_DIR, DB_NAME)


class CertifyingAuthority:
    @staticmethod
    def generate_keys():
        private_key, _ = generate_and_save_keys(
            1024,
            os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "CA_public_key.pem"),
            os.path.join(CA_DIR, "CA_private_key.pem"),
        )
        return private_key

    @staticmethod
    def issue_certificate(voter_identity, voter_public_key, CA_private_key):
        """
        Выдаёт сертификат в виде словаря в json-формате с 3 ключами:
        identity - личность избирателя,
        public_key - открытый ключ избирателя,
        signature - подпись CA на первые 2 поля
        :param voter_identity: личность избирателя
        :param voter_public_key: открытый ключ избирателя
        :param CA_private_key: приватный ключ CA
        :return: сертификат
        """

        # подписывает личность и открытый ключ избирателя
        signature = sign_message(
            voter_identity.encode() + voter_public_key, CA_private_key
        )

        return {
            "identity": voter_identity,
            "public_key": base64.b64encode(voter_public_key).decode("utf-8"),
            "signature": base64.b64encode(signature).decode("utf-8"),
        }

    @staticmethod
    def check_voter_mark_duplicates(salted_voter_mark):
        """
        Возвращает дубликаты соленых меток избирателей
        :param salted_voter_mark: соленая метка избирателя
        :return:
        """
        select_query = "SELECT * FROM CA_voters WHERE salted_voter_mark = ?;"

        db = Database(DB)
        db.execute_with_params(select_query, (salted_voter_mark,))
        return db.fetchall()

    @staticmethod
    def sign_voter_mark(salted_voter_mark, CA_private_key):
        """
        Подписывает соленую метку избирателя.
        :param salted_voter_mark: соленая метка избирателя
        :param CA_private_key: закрытый ключ CA
        :return:
        """
        return str(pow(salted_voter_mark, CA_private_key.d, CA_private_key.n))

    @staticmethod
    def create_voters_table():
        """
        Создаёт таблицу с избирателями, если её не существует.
        :return:
        """

        db = Database(DB)
        db.execute("DROP TABLE IF EXISTS CA_voters;")
        db.execute(
            """CREATE TABLE CA_voters (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            identity TEXT NOT NULL,
            salted_voter_mark TEXT NOT NULL,
            mark_digest TEXT NOT NULL,
            signed_voter_mark TEXT NOT NULL
            );"""
        )

    @staticmethod
    def add_new_voter(identity, salted_voter_mark, mark_digest, signed_voter_mark):
        """
        Сохраняет в БД личности и серийные номера избирателей.
        :param identity: ФИО
        :param salted_voter_mark: соленая метка избирателя
        :return:
        """

        insert_query = """
                INSERT INTO CA_voters(identity, salted_voter_mark, mark_digest, signed_voter_mark) VALUES (?, ?, ?, ?);
            """

        db = Database(DB)
        db.execute_with_params(
            insert_query,
            (
                identity,
                salted_voter_mark,
                base64.b64encode(mark_digest).decode("utf-8"),
                signed_voter_mark,
            ),
        )
        db.commit()

    @staticmethod
    def get_voters():
        """
        Извлекает из таблицы избирателей все записи.
        :return:
        """

        select_query = "SELECT identity, salted_voter_mark, mark_digest, signed_voter_mark FROM CA_voters;"

        db = Database(DB)
        db.execute(select_query)
        return db.fetchall()
