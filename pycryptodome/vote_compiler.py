import base64
import hashlib
import os

from db import Database
from helpers import get_value

VC_DIR = get_value("VC_dir")
DB_NAME = get_value("db_name")
DB = os.path.join(VC_DIR, DB_NAME)


class VoteCompiler:
    @staticmethod
    def check_vote(vote, signed_voter_mark, received_digest):
        """
        Проверяет, что голос не подделан.
        :param vote: голос
        :param signed_voter_mark: подписанная метка избирателя
        :param received_digest: полученный вместе с сообщением дайджест
        :return:
        """
        digest = hashlib.md5((vote + signed_voter_mark).encode("utf-8")).hexdigest()
        if received_digest.decode("utf-8") == digest:
            return True
        return False

    @staticmethod
    def check_voter_mark_duplicates(signed_voter_mark):
        """
        Возвращает дубликаты подписанных меток избирателей
        :param signed_voter_mark: подписанная метка избирателя
        :return:
        """
        select_query = "SELECT * FROM VC_voters WHERE signed_voter_mark = ?;"

        db = Database(DB)
        db.execute_with_params(select_query, (signed_voter_mark,))
        return db.fetchall()

    @staticmethod
    def create_voters_table():
        """
        Создаёт таблицу с избирателями, если её не существует.
        :return:
        """

        db = Database(DB)
        db.execute("DROP TABLE IF EXISTS VC_voters;")
        db.execute(
            """CREATE TABLE VC_voters (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            signed_voter_mark TEXT NOT NULL,
            vote TEXT NOT NULL,
            digest TEXT NOT NULL);"""
        )

    @staticmethod
    def add_new_voter(signed_voter_mark, vote, digest):
        """
        Сохраняет в БД подписанные метки избирателей.
        :param signed_voter_mark: подписанная метка избирателя
        :return:
        """

        insert_query = (
            "INSERT INTO VC_voters (signed_voter_mark, vote, digest) VALUES (?, ?, ?);"
        )

        db = Database(DB)
        db.execute_with_params(
            insert_query,
            (signed_voter_mark, vote, base64.b64encode(digest).decode("utf-8")),
        )
        db.commit()

    @staticmethod
    def get_voters():
        """
        Извлекает из таблицы избирателей все записи.
        :return:
        """

        select_query = "SELECT signed_voter_mark, vote, digest FROM VC_voters;"

        db = Database(DB)
        db.execute(select_query)
        return db.fetchall()
