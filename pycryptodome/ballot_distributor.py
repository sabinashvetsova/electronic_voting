import base64
import hashlib
import os

import Crypto.Random.random as random

from db import Database
from helpers import get_value
from pycryptodome.crypto_helpers import (
    generate_and_save_keys,
    verify_message_signature,
    sign_message,
)

BD_DIR = get_value("BD_dir")
COMMON_DIR = get_value("common_dir")
PUBLIC_KEYS_DIR = get_value("public_keys_dir")
DB_NAME = get_value("db_name")
DB = os.path.join(BD_DIR, DB_NAME)


class BallotDistributor:
    @staticmethod
    def generate_keys():
        private_key, _ = generate_and_save_keys(
            1024,
            os.path.join(COMMON_DIR, PUBLIC_KEYS_DIR, "BD_public_key.pem"),
            os.path.join(BD_DIR, "BD_private_key.pem"),
        )
        return private_key

    @staticmethod
    def check_certificate(certificate, ca_public_key):
        """
        Проверяет, что сертификат валиден.
        :param certificate: сертификат
        :param ca_public_key: открытый ключ CA
        :return: True, если сертификат валиден. False - в обратном случае.
        """

        voter_public_key = base64.b64decode(str.encode(certificate.get("public_key")))
        signature = base64.b64decode(str.encode(certificate.get("signature")))

        message = certificate.get("identity", "").encode() + voter_public_key

        return verify_message_signature(message, signature, ca_public_key)

    @staticmethod
    def generate_serial_numbers(count):
        """
        Генерирует уникальные серийные номера y для избирателей.
        :param count: кол-во избирателей
        :return: список серийных номеров
        """

        return random.sample(range(1, 100 * count), count)

    def get_ballot(self, serial_number, BD_private_key):
        """
        Возвращает пустой бюллетень в виде словаря в json-формате с 3 ключами:
        serial_number - серийный номер y,
        digest - хеш от y,
        signature - подпись BD на первые 2 поля
        :param serial_number: серийный номер y
        :param BD_private_key: приватный ключ BD
        :return: бюллетень
        """

        # берёт хеш от серийного номера
        bytes_string = str(serial_number).encode()
        digest = hashlib.md5(bytes_string).hexdigest()

        # подписывает хеш
        signature = sign_message(digest.encode("utf-8"), BD_private_key)

        return {
            "serial_number": serial_number,
            "digest": digest,
            "signature": base64.b64encode(signature).decode("utf-8"),
        }

    @staticmethod
    def create_voters_table():
        """
        Создаёт таблицу с избирателями, если её не существует.
        :return:
        """

        db = Database(DB)
        db.execute("DROP TABLE IF EXISTS BD_voters;")
        db.execute(
            """CREATE TABLE BD_voters (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            identity TEXT NOT NULL,
            serial_number INTEGER NOT NULL);"""
        )

    @staticmethod
    def add_new_voters(identities, serial_numbers):
        """
        Сохраняет в БД личности и серийные номера избирателей.
        :param identities: список ФИО
        :param serial_numbers: список серийных номеров
        :return:
        """

        voters = []
        insert_query = "INSERT INTO BD_voters (identity, serial_number) VALUES (?, ?);"
        db = Database(DB)

        for i in range(len(serial_numbers)):
            voters.append((identities[i], serial_numbers[i]))

        db.executemany(insert_query, voters)
        db.commit()

    @staticmethod
    def get_voters():
        """
        Извлекает из таблицы избирателей все записи.
        :return:
        """

        select_query = "SELECT identity, serial_number FROM BD_voters;"

        db = Database(DB)
        db.execute(select_query)
        return db.fetchall()
