from OpenSSL import crypto
from cryptography.hazmat.primitives import serialization

from helpers import write_bytes


def generate_and_save_keys(keys_length, public_key_path, private_key_path):
    """
    Генериует открытый и закрытый ключи и сохраняет их в файлах в PEM-формате.
    :param keys_length: длина ключей
    :param public_key_path: путь до файла с открытым ключом
    :param private_key_path: путь до файла с закрытым ключом
    :return: объект с закрытым ключом и открытый ключ в PEM-формате
    """

    key = crypto.PKey()
    key.generate_key(crypto.TYPE_RSA, keys_length)
    private_key = key.to_cryptography_key()

    pem_private_key = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption(),
    )

    public_key = private_key.public_key()

    pem_public_key = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )

    write_bytes(pem_public_key, public_key_path)
    write_bytes(pem_private_key, private_key_path)

    return key, pem_public_key
