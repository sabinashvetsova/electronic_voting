import base64
import os
import time

from OpenSSL import crypto

from helpers import get_value, write_json
from pyOpenSSL.helpers import generate_and_save_keys

BASE_KEYS_DIR = get_value("base_keys_dir")
VOTERS_KEYS_DIR = get_value("voters_keys_dir")
VOTERS_CERTIFICATES_DIR = get_value("voters_certificates_dir")
NUMBER_OF_VOTERS = get_value("number_of_voters")


class CertifyingAuthority:
    def __init__(self):
        self.private_key, _ = generate_and_save_keys(
            2048,
            os.path.join(BASE_KEYS_DIR, "CA_public_key.pem"),
            os.path.join(BASE_KEYS_DIR, "CA_private_key.pem"),
        )

    def issue_certificate(self, voter_identity, voter_public_key):
        """
        Выдаёт сертификат в виде словаря в json-формате с 3 ключами:
        identity - личность избирателя,
        public_key - открытый ключ избирателя,
        signature - подпись CA на первые 2 поля
        :param voter_identity: личность избирателя
        :param voter_public_key: открытый ключ избирателя
        :return: сертификат
        """

        # подписывает личность и открытый ключ избирателя
        signature = crypto.sign(
            self.private_key, voter_identity.encode() + voter_public_key, "MD5"
        )
        return {
            "identity": voter_identity,
            "public_key": base64.b64encode(voter_public_key).decode("utf-8"),
            "signature": base64.b64encode(signature).decode("utf-8"),
        }


if __name__ == "__main__":
    start = time.time()
    os.makedirs(os.path.join(BASE_KEYS_DIR, VOTERS_KEYS_DIR), exist_ok=True)
    os.makedirs(VOTERS_CERTIFICATES_DIR, exist_ok=True)

    CA = CertifyingAuthority()

    identities = get_value("identities", "../identities.json")

    for number in range(NUMBER_OF_VOTERS):
        _, pem_public_key = generate_and_save_keys(
            2048,
            os.path.join(
                BASE_KEYS_DIR, VOTERS_KEYS_DIR, f"voter_{number}_public_key.pem"
            ),
            os.path.join(
                BASE_KEYS_DIR, VOTERS_KEYS_DIR, f"voter_{number}_private_key.pem"
            ),
        )

        certificate = CA.issue_certificate(identities[number], pem_public_key)
        write_json(
            certificate,
            os.path.join(VOTERS_CERTIFICATES_DIR, f"voter_{number}_certificate.json"),
        )

    print("It took", time.time() - start, "seconds.")
